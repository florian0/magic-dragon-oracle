import discord
import jfx_bridge
import logging
import jinja2
from app.client import client
from discord import app_commands
from app.bridge import lookup_symbol

logger = logging.getLogger("commands.query")
logger.setLevel(logging.INFO)

template = jinja2.Template("""
{% if symbol is none %}
I got no clue what that is
{% else %}
{% if func is none %}
The magic dragon knows this as `{{ symbol_name }} ` at `{{ symbol_address }}`
{% else %}
The magic dragon tells this is `{{ func_name }}` at `{{ func_entry_point }}`
```cpp
{{ func_signature }}
```
{% endif %}
{% endif %}
""")


@client.tree.command()
@app_commands.describe(
    address='Address to query'
)
async def query(interaction: discord.Interaction, address: str):
    """
    Query magic data from a mysterious source
    """
    logger.info(interaction.user)
    logger.info(address)
    try:
        logger.info(f"User {interaction.user.name} queried for address {address}")
        symbol_name, symbol_address, func_name, func_entry_point, func_signature = lookup_symbol(address)

        await interaction.response.send_message(
            template.render({
                'symbol_name': symbol_name,
                'symbol_address': symbol_address,
                'func_name': func_name,
                'func_entry_point': func_entry_point,
                'func_signature': func_signature
            })
        )

    except jfx_bridge.bridge.BridgeException as ex:
        logger.error("Bridge threw an exception")
        logger.error(ex)
