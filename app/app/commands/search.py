import discord
import jfx_bridge
import logging
import jinja2
from itertools import islice
from app.client import client
from discord import app_commands
from app.bridge import find_symbols

logger = logging.getLogger("commands.search")

template = jinja2.Template("""
{% for (name, address) in results %}
* `{{ name }}` at `{{ address }}`
{% else %}
:dragon_face: ... *Silence* ...
{% endfor %}
""")


@client.tree.command()
@app_commands.describe(
    query_string='Address to query'
)
async def search(interaction: discord.Interaction, query_string: str):
    """
    Query magic data from a mysterious source
    """
    try:
        results = find_symbols(query_string)

        await interaction.response.send_message(
            template.render({
                'results': islice(results, 25)
            })
        )

    except jfx_bridge.bridge.BridgeException as ex:
        logger.error("Bridge threw an exception")
        logger.error(ex)
