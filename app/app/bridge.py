import ghidra_bridge
from app.config import GHIDRA_HOST
import typing

if typing.TYPE_CHECKING:
   import ghidra
   from ghidra.ghidra_builtins import *
else:
    b = ghidra_bridge.GhidraBridge(connect_to_host=GHIDRA_HOST, namespace=globals())


def is_address_expression(input_string: str):
    return any(c in input_string for c in ['+', '-', '*'])


# def lookup_symbol(address: str):
#     ...


# def lookup_symbol_by_address(address):
#     ...


# def find_symbols(input_string: str):
#     ...

