import importlib
import discord
import os
from jfx_bridge.bridge import DEFAULT_HOST

from dotenv import load_dotenv

load_dotenv()

MY_GUILD = discord.Object(id=int(os.getenv('GUILD')))
TOKEN = os.getenv('DISCORD_TOKEN')
GHIDRA_HOST = os.getenv("GHIDRA_HOST") or DEFAULT_HOST
DRAGON_ROLE_ID = int(os.getenv("DRAGON_ROLE_ID")) or 0
