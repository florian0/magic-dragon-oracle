import logging
import os
import ghidra_bridge

from app.client import client
from app.config import TOKEN
import app.commands  # keep this import, otherwise we have no commands :(

logger = logging.getLogger("app")

if __name__ == '__main__':
    client.run(TOKEN)
